const Movie = require('../models/movie');
const utils = require('../util/funtions');
const service = require('../services/index');

var log4js = require('log4js');
var loggerApp = log4js.getLogger("app");

module.exports = class MovieTask {
    static list(ctx) {
        var pageh = ctx.get('page');
        var page;
        var limit = 2;
        if (pageh == null || pageh.trim().length < 1) {
            page = 1
        } else {
            page = Number.parseInt(pageh);
            if (Number.isNaN(page)) {
                loggerApp.error('the page(' + pageh + ') field must be numeric.');
                return utils.errorResponse('The page field must be numeric.', 400, ctx);
            }
        }
        if (page < 1) {
            loggerApp.error('Cannot use page with value ' + page + '.');
            return utils.errorResponse('The page must be greater than 0.', 400, ctx);
        }
        page = (page - 1) * limit;
        loggerApp.info('Completed request to return films.');
        return Movie.find().skip(page).limit(limit).lean().then(ctx.ok, ctx.badRequest);
    }

    static async view(ctx) {
        var filter;
        var yearPar = ctx.get('year');
        if (yearPar == null || yearPar.trim().length < 1) {
            filter = { Title: { '$regex': '.*^' + ctx.params.title + '.*', '$options': 'i' } };
        } else {
            if (Number.isNaN(Number.parseInt(yearPar))) {
                loggerApp.error('invalid format of the year(' + yearPar + ') field.');
                return utils.errorResponse('Invalid year format.', 400, ctx);
            }
            filter = { Title: { '$regex': '.*^' + ctx.params.title + '.*', '$options': 'i' }, Year: yearPar };
        }

        return Movie.findOne(filter).sort({ Year: -1 }).limit(1).lean().then(rslt => {
            if (rslt == null) {
                loggerApp.warn('Movie(' + ctx.params.title + ') not found.');
                return utils.errorResponse('Movie not found', 404, ctx);
            } else {
                ctx.body = rslt;
                ctx.status = 200;
                loggerApp.info('Completed request to return movie.');
                return ctx;
            }
        }).catch(err => {
            loggerApp.error('Internal Error:', err);
            return utils.errorResponse(500, 'An unexpected internal error has occurred, please contact the support team.');
        });
    }

    static updatePlot(ctx) {
        var movie = ctx.request.body.movie;
        var find = ctx.request.body.find;
        var replace = ctx.request.body.replace;
        if (movie == null || movie.trim().length < 0) {
            loggerApp.error('The movie field is empty.');
            return utils.errorResponse('The movie parameter must exist and cannot be empty.', 400, ctx);
        }
        if (find == null || find.trim().length < 0) {
            loggerApp.error('The find field is empty.');
            return utils.errorResponse('The find parameter must exist and cannot be empty.', 400, ctx);
        }
        if (replace == null || replace.trim().length < 0) {
            loggerApp.error('The replace field is empty.');
            return utils.errorResponse('The replace parameter must exist and cannot be empty.', 400, ctx);
        }

        var filter = { Title: { '$regex': '^' + movie + '$', '$options': 'i' } };
        return Movie.findOne(filter).sort({ Year: -1 }).limit(1).then(rslt => {
            if (rslt == null) {
                loggerApp.warn('Movie(' + movie + ') not found.');
                return utils.errorResponse('Movie not found', 404, ctx);
            } else {
                var myPlot = rslt.Plot;
                if (rslt.Plot != null) {
                    rslt.Plot = myPlot.replaceAll(find, replace);
                    rslt.save();
                }
                ctx.body = { plot: rslt.Plot }; ctx.status = 200;
                loggerApp.info('Completed request to update plot.');
                return ctx;
            }

        }).catch(err => {
            loggerApp.error('Internal Error:', err);
            return utils.errorResponse(500, 'An unexpected internal error has occurred, please contact the support team.');
        });

    }

    static getToken(ctx) {
        try {
            var tokenRes = service.createToken(ctx.request.body);
            if (tokenRes == null) {
                loggerApp.error('The id field must exist and cannot be empty');
                return utils.errorResponse('The id field must exist and cannot be empty.', 400, ctx);
            }
            ctx.body = { token: tokenRes };
            ctx.status = 201;
            loggerApp.info('completed request to create token.');
            return ctx;
        } catch (error) {
            loggerApp.error('Internal Error:', err);
            return utils.errorResponse(500, 'An unexpected internal error has occurred, please contact the support team.');
        }

    }
}