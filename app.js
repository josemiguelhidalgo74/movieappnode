const Koa = require('koa');
const KoaRouter = require('koa-router');
const KoaRespond = require('koa-respond');
const koaBody = require('koa-body');
const config = require('./config/config');

const app = new Koa();
const router = new KoaRouter();
const mdb = require('./integration/mongodb');
mdb.connect();
app.use(KoaRespond());
app.use(koaBody());
require('./routers/signatures')(router);
app.use(router.routes());
app.use(router.allowedMethods());

const swagger = require("swagger2");
const { ui, validate } = require("swagger2-koa");
const swaggerDocument = swagger.loadDocumentSync("./api.yml");
app.use(ui(swaggerDocument, "/docs"));

const log4js = require('log4js');
let logConfigPath = './config/log4js.json';
let logConfig = require(logConfigPath);
log4js.configure(logConfig);
var logger = log4js.getLogger("app");


app.listen(config.PORT, () => {
  logger.info('Server ready!!!!.');
});