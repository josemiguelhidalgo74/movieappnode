const mongoose = require('mongoose');
const movieSchema = new mongoose.Schema({
    Title: String,
    Year: String,
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: [{
        Source: String,
        Value: String
    }]
});

module.exports = mongoose.model('Movie', movieSchema);