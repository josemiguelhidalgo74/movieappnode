module.exports = {
    PORT: process.env.PORT || 3000,
    MONGO_URI: process.env.MONGO || 'mongodb://localhost:27017/movie',
    SECRET_TOKEN: process.env.SECRET_TOKEN || 'mytoken'
}