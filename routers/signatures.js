const KoaRouter = require('koa-router');
const controllers = require('../controllers/movie');
const middlewares = require('../middlewares/middlewares');

module.exports = router => {
    router.get('/v1/movies', middlewares.isAut, controllers.list)
        .get('/v1/movies/:title', middlewares.isAut, controllers.view)
        .post('/v1/movies', middlewares.isAut, controllers.updatePlot)
        .post('/v1/users', controllers.getToken)
}

