const mongoose = require('mongoose');
const config = require('../config/config');

module.exports = class MongoDB {
    static connect() {
        return mongoose.connect(config.MONGO_URI, {
            useNewUrlParser: true
        });
    }
}