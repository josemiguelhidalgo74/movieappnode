# MovieAppNode

Aplicación en NodeJs con CRUD simple para administras Películas. 

## Componentes utilizados

- Implementación con koa
- Autenticación basada en token con jwt
- Documentación con swagger
- Conexión con Mongodb usando mongoose
- Registros de logs usando log4j

## Configuración 
Los parámetros de configuración se encuentren en config/config.js

Descripción de parámetros:

- PORT: Puerto donde levanta la aplicacion 
- MONGO_URI: Url para conectar con la db de mongo
- SECRET_TOKEN: Clave para cifrar los token

Nota: Al establecer un puerto diferente al 3000, se debe actualizar el campo url del archivo api.yml.

## Instrucciones para iniciar la aplicación

- Descargar proyecto: git clone https://gitlab.com/josemiguelhidalgo74/movieappnode.git
- Moverse al directorio: movieappnode
- Ejecutar: npm start
- Ir a: http://localhost:3000/docs

## Instrucciones de Uso

- Solicitar token con el metodo POST http://localhost:3000/v1/users, este método requiere un body. PE:{"id":"user@demo.com"}
- Para solicitar cualquier método del API, usar como autenticación el token previamente creado

