const utils = require('../util/funtions');
const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config/config');

var log4js = require('log4js');
var loggerAccess = log4js.getLogger("http");

module.exports = {
    isAut: (ctx, next) => {
        loggerAccess.info(ctx.request.method + ' ' + ctx.request.header.host + ctx.request.url)
        const autHeader = ctx.headers.authorization;
        if (autHeader == null) {
            loggerAccess.error('Unauthorized user.');
            return utils.errorResponse('Unauthorized user.', 401, ctx);
        }
        try {
            const token = autHeader.split(' ')[1];
            const payload = jwt.decode(token, config.SECRET_TOKEN);

            if (payload.exp < moment().unix()) {
                loggerAccess.error('Token has expired.');
                return utils.errorResponse('Token has expired.', 401, ctx);
            }
            return next();
        } catch (error) {
            loggerAccess.error('Internal Error:', error);
            return utils.errorResponse('User not valid.', 403, ctx);
        }

    }
}