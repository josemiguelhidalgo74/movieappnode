const jwt = require('jwt-simple');
const moment = require('moment');
const config = require('../config/config');

module.exports = {
    createToken: (user) => {
        if (user.id == null || user.id.trim().length < 1)
            return null;
        const payload = {
            sub: user.id,
            iat: moment().unix(),
            exp: moment().add(14, 'days').unix()
        }
        return jwt.encode(payload, config.SECRET_TOKEN);
    }
}